<?php
	if(!isset($_POST['busqueda'])){
		header("Location: index.php");
	}

?>
<?php require_once 'includes/cabecera.php'; ?> <!-- minimo para una pantalla  -->
<?php require_once 'includes/lateral.php'; ?> <!-- minimo para una pantalla  -->
<?php 
    $busqueda = isset($_POST['busqueda']) ? $_POST['busqueda']:false;
    if(!empty($busqueda)):
?>		
<!-- CAJA PRINCIPAL -->
<div id="principal"> <!-- minimo para una pantalla  -->

	<h1>Busqueda: <?=$busqueda?></h1>
	
	<?php 
		$entradas = conseguirEntradas($db, null, null, $busqueda);

		if(!empty($entradas) && mysqli_num_rows($entradas) >= 1):
			while($entrada = mysqli_fetch_assoc($entradas)):
	?>
				<article class="entrada">
					<a href="entrada.php?id=<?=$entrada['id']?>">
						<h2><?=$entrada['titulo']?></h2>
						<span class="fecha"><?=$entrada['categoria'].' | '.$entrada['fecha']?></span>
						<p>
							<?=substr($entrada['descripcion'], 0, 180)."..."?>
						</p>
					</a>
				</article>
	<?php
			endwhile;
		else:
	?>
		<div class="alerta">No hay entradas en esta categoría</div>
	<?php endif;
            else: ?>
                <div class="alerta">La busqueda es vacia</div>
        <?php endif; ?>
</div> <!--fin principal-->
			
<?php require_once 'includes/pie.php'; ?><!-- minimo para una pantalla  -->