<?php
if(isset($_POST)){
    // conexion a la base de datos
    require_once './includes/conexion.php';
    
    $nombre = isset($_POST['nombre']) ? mysqli_real_escape_string($db, $_POST['nombre']): false;
    
    // arreglo de errores
    $errores = array();
    
    if(!empty($nombre) && !is_numeric($nombre) && !preg_match("/[0-9]/", $nombre)){
        $nombre_valido = true;
    }else{
        $nombre_valido = false;
        $errores['nombre'] = "El nombre ingresado no es valido";
    }
    
    if(count($errores) == 0){
        $sql = "INSERT INTO categorias values(null,'$nombre');";
        $guardar = mysqli_query($db, $sql);
        $_SESSION['correcto'] = "Se ha creado la categoria.";
    }else{
        $_SESSION['errores'] = $errores['nombre'];
    }
}
header("Location: crear-categoria.php");
