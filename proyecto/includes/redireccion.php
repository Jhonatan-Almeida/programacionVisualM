<?php

if(!isset($_SESSION)){
	session_start();
}

if(!isset($_SESSION['usuario'])){
    echo "<script type='text/javascript'>alert('No puede ingresar a esta página, sin logearse');</script>";
    //die();
//header("Location: index.php");
    header("Refresh:0; url=error-login.php");
}