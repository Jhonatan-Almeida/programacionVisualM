<?php require_once 'includes/redireccion.php'; ?>
<?php require_once 'includes/cabeceraSecundaria.php'; ?>	
<?php require_once 'includes/lateral.php'; ?>

<!-- CAJA PRINCIPAL -->
<div id="principal">
	 <h3>CREAR CATEGORIA</h3>
        <p>Ingresar el nombre de la categoria a crear</p><br>
        <form action="guardar-categoria.php" method="POST">
        <?php  if(isset($_SESSION['correcto'])): ?>
            <div class="alerta alerta-exito">
                <?= ($_SESSION['correcto']); ?>
            </div>
        <?php  elseif(isset($_SESSION['errores'])): ?>
                <div class="alerta alerta-error">
                    <?= ($_SESSION['errores']); ?>
                </div>
        <?php endif; ?>
        <label for="nombre">Nombre categoria</label>
        <input type="text" name="nombre">
        <input type="submit" value="Guardar">
            
    </form>

</div> <!--fin principal-->
			
<?php require_once 'includes/pie.php'; ?>